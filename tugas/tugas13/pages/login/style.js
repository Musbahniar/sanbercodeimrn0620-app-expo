import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: 'white',
    // paddingTop: Constants.statusBarHeight,
  },
  navBar: {
    height: 30,
    backgroundColor: '#25A8E0'
  },
  textTile: {
    color: '#F7E23B',
    fontWeight: 'bold',
    fontSize: 30,
    paddingTop: 117,
    textAlign: 'center'
  },
  textSubTile: {
    color: '#25A8E0',
    fontSize: 15,
    paddingTop: 8,
    paddingLeft: 12,
    paddingRight: 12,
    textAlign: 'center'
  },
  textEmail: {
    paddingLeft: 12,
    paddingRight: 12,
    borderWidth: 1,
    borderColor: 'grey'
  },
  containerForm: {
    flex: 1,
    paddingTop: 50
  }

});

export default styles;