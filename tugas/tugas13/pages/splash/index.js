import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import styles from './style';

export default class Splashcreen extends Component {
  render() {
    setTimeout(() => {
      this.props.navigation.navigate('login');
      }, 1000);

    return (
      <View style={styles.container}>
          <Image source={require('../../assets/logo.png')} style={{width:200, height:200, marginTop:170}} />
          <Text style={styles.textTile}>sanbercode My portofolio</Text>
          <Text style={styles.textSubTile}>Update your paortfolio, skills And get project opportunities that fit your interests.</Text>
          <View style={styles.progressBar}></View>
      </View>
    )
  }
}

