import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#1A3665',
    paddingTop: Constants.statusBarHeight,
  },
  textTile: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    paddingTop: 40
  },
  textSubTile: {
    color: 'white',
    fontSize: 12,
    paddingTop: 10,
    paddingLeft: 12,
    paddingRight: 12,
    textAlign: 'center'
  },
  progressBar: {
    height: 6,
    width: '100%',
    backgroundColor: 'white',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 5
  }
});

export default styles;