import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './style';

export default class Videoitem extends Component {
    render() {
        let video = this.props.video;
        // alert(video.id);
        return (
          <View style={styles.container}>
            <Image source={{uri:video.snippet.thumbnails.medium.url}} style={{height: 200}} />
            <View style={styles.desContainer}>
              <Image source={{uri:'https://cdn.pixabay.com/photo/2014/10/02/00/15/gas-mask-469217_960_720.jpg'}} style={{width:50, height:50, borderRadius:25}} />
              <View style={styles.videoDetail}>
                <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
                <Text style={styles.videoStat}>
                  {video.snippet.channelTitle + " · " + nFormatter(video.statistics.viewCount)}
                </Text>
            </View>
            <TouchableOpacity>
              <Icon name="more-vert" size={20} />
            </TouchableOpacity>
            </View>
           
          </View>
        )
    }
}

function nFormatter(num, digits) {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' view';
}


