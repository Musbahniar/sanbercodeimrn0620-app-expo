import { StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  desContainer: {
    flexDirection: 'row',
    paddingTop: 15
  },
  videoTitle: {
    fontSize: 14,
    color: '#3c3c3c'
  },
  videoDetail: {
    paddingHorizontal: 15,
    flex: 1
  },
  videoStat: {
    fontSize: 12,
    paddingTop: 3
  }
});

export default styles;