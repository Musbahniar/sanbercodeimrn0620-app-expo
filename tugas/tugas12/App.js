import React from 'react';
import { Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItems from './component/videoitem';
import data from './data.json';
import styles from './style';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
          <Image source={require('./images/logoyoutube.png')} style={{width:99, height:22}} />
          <View style={styles.rightNavBar}>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="search" size={25} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="account-circle" size={25} />
            </TouchableOpacity>
          </View> 
      </View>
      <View style={styles.body}>
          {/* <VideoItems video={data.items[0]}/> */}
        <FlatList 
          data={data.items}
          renderItem={(video)=><VideoItems video={video.item} />}
          keyExtractor={(item)=>item.id}
          ItemSeparatorComponent={()=> <View style={{height:1, backgroundColor:'#E5E5E5'}}/>}
        />
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItems}>
          <Icon name="home" size={25} />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItems}>
          <Icon name="whatshot" size={25} />
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItems}>
          <Icon name="subscriptions" size={25} />
          <Text style={styles.tabTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItems}>
          <Icon name="folder" size={25} />
          <Text style={styles.tabTitle}>Libary</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

